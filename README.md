# Tipie Challenge

## Frontend guide
### 1. Installation
**This guide assumes a working installation of Node.js in your system, and a basic understanding of terminal operation.**

First, clone the repo with `git clone https://gitlab.com/Delpi/tipie-challenge-frontend.git`.

Then, `cd` into your working directoy, and run `npm install` to update all relevant Node packages.

### 2. Initial configuration
Open the `.env` file in any text editor, and modify the location of the backend stored there. It must **not** include trailing slashes.

Finally, do `npm run build` to obtain a production build of the site.

### 3. Deployment
Copy the files in the `build` folder into the home directory of the website.

## Operation
Assuming a correct installation, the webapp will render a loading page. There it will attempt to fetch the table from the server.

If the table has been fetched, it will render it. The user can click on any column headers to sort it both in ascending and descending alphabetical order.

If the table was not able to be fetched, the app will render a login form to try and authorize the user to retrieve the table.

## Design constraints
The components of the app, as well as the backend interation API have written with expansion in mind.

Should the names of the columns, or its management in-server be changed, they can be altered at the `src/views/tableView.jsx` file.
