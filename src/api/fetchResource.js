import fetchWrap from './fetchWrap.js';

export default async function fetchResource (resource, data) {
    const resourceIndex = {
        login: {
            url: "login",
            method: "POST",
            body: {
                user: data?.user,
                password: data?.password
            }
        },
        table: {
            url: "table"
        },
        test: {
            url: "/"
        }
    };

    return await fetchWrap(
        resourceIndex[resource]
    );
}
