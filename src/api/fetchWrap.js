export default async function fetchWrap (data) {
    try {
        if (!data) {
            throw new Error("Invalid resource");
        }
        const url = process.env.REACT_APP_BACKEND + "/" + data.url;

        const response = await fetch(url, {
            method: data.method ?? 'GET',
            credentials: 'include',
            body: JSON.stringify(data.body),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.ok) {
            const body = await response.json();
            return {
                status: true,
                body: body.body
            };
        } else if (response.json) {
            const body = await response.json();
            throw new Error(
                body.status
            );
        } else {
            throw new Error(`Unable to fetch ${url}`);
        }
    } catch (err) {
        return {
            status: false,
            message: err.message
        };
    }
}
