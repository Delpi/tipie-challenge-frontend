import React, { useState } from 'react';

import TableHeader from './table/tableHeader.jsx';
import TableBody from './table/tableBody.jsx';

export default function Table (props) {
    // Table row keys
    let i = 0;
    const inputTable = props.data.map(element => {
        return {
            ...element,
            key: i++
        };
    });
    const [tableData, setTableData] = useState(inputTable);

    // Table order manager
    const [sortField, setSortField] = useState("key");
    const [sortDirection, setSortDirection] = useState("ascending");

    const toggleDirection = () => {
        if (sortDirection === "ascending") {
            setSortDirection("descending");
        } else {
            setSortDirection("ascending");
        }
    }

    const setOrderBy = column => {
        setSortField(column);
        if (column === sortField) {
            toggleDirection();
        }

        sortTable(
            tableData,
            column, // Fixes a bug where the order lags behind the form state
            sortDirection
        ).then(sorted => {
            setTableData(sorted);
        });
    };

    return (
        <table>
            <TableHeader orderBy={setOrderBy}
                         order={sortField}
                         columns={props.columns} />
            <TableBody columns={props.columns}
                       data={tableData} />
        </table>
    );
}

async function sortTable (table, field="key", direction="ascending") {
    const order = {
        descending: (a, b) => a < b,
        ascending: (a, b) => a > b
    };
    const fx = order[direction];

    const sorted = table.sort((a, b) => {
        if (fx(a[field], b[field])) {
            return 1;
        } else if (fx(b[field], a[field])) {
            return -1;
        } else {
            return 0;
        }
    });
    return sorted;
}
