export default function TableBody (props) {
    const rows = props.data.map(row => {
        const fields = props.columns.map(column => {
            return (
                <td key={column.key}>
                    {row[column.field]}
                </td>
            );
        });
        return (
            <tr key={row.key}>
                {fields}
            </tr>
        );
    });

    return (
        <tbody>
            {rows}
        </tbody>
    );
}
