export default function TableHeader (props) {
    let i = 0;
    const columns = props.columns.map(column => {
        const orderBy = () => {
            props.orderBy(column.field);
        };
        const highlight = column.field === props.order ?
                          "order" : "";
        return (
            <th key={i++}
                className={highlight}
                onClick={orderBy} >
                {column.label}
            </th>
        );
    });
    return (
        <thead>
            <tr>
                {columns}
            </tr>
        </thead>
    );
}
