import React, { useState } from 'react';

export default function PasswordField (props) {
    // Visibility manager
    const [isVisible, setIsVisible] = useState(false);
    const toggleVisible = () => {
        setIsVisible(!isVisible);
    }

    // Controlled value & update manager
    const [value, setValue] = useState(props.value ?? '');
    const onChange = (event) => {
        setValue(event.target.value);

        // NOTE: following code is meant for symmetry between this
        //       component and HTML-specified password fields, when
        //       extracting the value outside the component
        const valueExport = {
            target: {
                value: value
            }
        };
        if (props.onChange) {
            props.onChange(valueExport);
        }
    }

    // Focus alterations (for styling symmetry)
    const [isFocused, setIsFocused] = useState(false);
    const toggleFocus = (event) => {
        // NOTE: this prevents a bug that cuts off the last char of the field
        onChange(event);
        setIsFocused(!isFocused);
    };

    return (
        <div className={"passwordField" + (isFocused ? " active" : "")}>
            <input onChange={onChange}
                   type={isVisible ? "text" : "password"}
                   value={value}
                   onFocus={toggleFocus}
                   onBlur={toggleFocus}
            />
            <input onClick={toggleVisible}
                   type="checkbox"
                   checked={isVisible}
            />
        </div>
    );
}
