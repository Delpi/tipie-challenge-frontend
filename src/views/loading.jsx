import React, { useEffect } from 'react';
import fetchResource from '../api/fetchResource.js';
import './loading.css';

export default function Loading (props) {
    let viewManager = props.manager
    useEffect(() => {
        fetchResource("table")
            .then((res) => {
                if (!res.status) {
                    viewManager("view", "login");
                } else {
                    viewManager("table", res.body);
                    viewManager("view", "table");
                }
            });
    }, [viewManager]);
    return (
        <div className="mainView">
            <h1>Loading...</h1>
            <div className="outerSpinner">
                <div className="innerSpinner" />
            </div>
        </div>
    );
}
