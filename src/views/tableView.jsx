import Table from '../components/table.jsx';
import './table.css';

export default function TableView (props) {
    let i = 0;
    const columns = [
        {
            field: "name",
            label: "Nombre",
            key: i++
        },
        {
            field: "age",
            label: "Edad",
            key: i++
        },
        {
            field: "sector",
            label: "Sector",
            key: i++
        },
        {
            field: "email",
            label: "Email",
            key: i++
        }
    ];

    return (
        <div className="mainView">
            <Table data={props.viewState.table}
                   columns={columns} />
        </div>
    );
}
