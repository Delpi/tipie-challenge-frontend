import React, { useState } from 'react';
import fetchResource from '../api/fetchResource.js';
import PasswordField from '../components/passwordField.jsx';

import './login.css';

export default function Login (props) {
    // Form controllers
    const [user, setUser] = useState("demo@tipieapp.com");
    const [password, setPassword] = useState("");
    const updateUser = (event) => {
        setUser(event.target.value);
    };
    const updatePassword = (event) => {
        setPassword(event.target.value);
    };

    const [formActive, setFormActive] = useState(true);
    const [formStatus, setFormStatus] = useState("¡Bienvenido! Por favor ingrese sus datos");

    const formSubmit = (event) => {
        event.preventDefault();
        if (!formActive) {
            return;
        }
        setFormActive(false);
        const formData = {
            user: user.trim(),
            password: password.trim()
        };

        setFormStatus("Iniciando sesión...");
        fetchResource("login", formData)
            .then(res => {
                if(!res.status) {
                    setFormActive(true);
                    setFormStatus(
                        res.message === "Unable to log in" ?
                        "Datos incorrectos" : "Error desconocido"
                    );
                } else {
                    setFormStatus("Bienvenido!");
                    props.manager("view", "loading");
                }
            });
    };

    return (
        <div className="mainView">
            <h1>{formStatus}</h1>
            <form>
                <label>
                    Usuario
                    <input type="text"
                           value={user}
                           onChange={updateUser} />
                </label>
                <label>
                    Contraseña
                    <PasswordField value={password}
                                   onChange={updatePassword} />
                </label>
                <button onClick={formSubmit}>
                    Iniciar sesión
                </button>
            </form>
        </div>
    );
}
