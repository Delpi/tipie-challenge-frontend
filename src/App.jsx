import React, { useState } from 'react';

import Loading from './views/loading.jsx';
import Login from './views/login.jsx';
import TableView from './views/tableView.jsx';

import './App.css';

export default function App() {
    // View state manager
    const [viewState, setViewState] = useState({
        view: "loading"
    });
    const alterViewState = (field, value) => {
        setViewState((prev) => {
            return {
                ...prev,
                [field]: value
            }
        });
    };

    // View render manager
    let View;
    switch (viewState.view) {
        case "login": {
            View = (
                <Login manager={alterViewState} />
            );
            break;
        }
        case "table": {
            View = (
                <TableView manager={alterViewState}
                           viewState={viewState} />
            );
            break;
        }
        case "loading":
        default: {
            View = (
                <Loading manager={alterViewState}
                         viewState={viewState} />
            );
            break;
        }
    }

    return (
        <div className="App">
            {View}
        </div>
    );
}
